all: mandrill.phar

mandrill.phar: mkphar.php
	php -d phar.readonly=0 mkphar.php

test: mandrill.phar
	php testphar.php