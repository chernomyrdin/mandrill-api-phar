<?php

if (!is_dir('mandrill-api-php')) die("Missing «mandrill-api-php» directory");
chdir('mandrill-api-php');

if (!is_file('composer.json')) die("Missing «composer.json» file");
if (!is_dir('src')) die("Missing «src» directory");

$phar = new Phar("../mandrill.phar");
$phar->setAlias("Mandrill");
$phar->setMetadata(json_decode(file_get_contents('composer.json'), TRUE));
/* To enable autoload, add below code before try statement
	spl_autoload_register(
		function ($class) {
			if (strpost($class, "Mandrill_") !== 0) return FALSE;
			include_once "phar://Mandrill/Mandrill/$class.php";
		}
	);
 */
$phar->setStub(
	'<?php
	try {
		Phar::mapPhar();
		include_once "phar://Mandrill/Mandrill.php";
	}
	catch (PharException $e) {
		error_log("$e");
		exit(1);
	}
	__HALT_COMPILER();'
);
$phar->buildFromDirectory('src');

# [EOF]
